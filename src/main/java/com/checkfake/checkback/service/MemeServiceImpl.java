package com.checkfake.checkback.service;

import com.checkfake.checkback.dto.MemeDto;
import com.checkfake.checkback.entity.Meme;
import com.checkfake.checkback.exceptions.BadRequestMeme;
import com.checkfake.checkback.repository.MemeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

@Service
@Qualifier(value= "MemeServiceImpl2")
public class MemeServiceImpl implements MemeService {

    @Autowired
    private MemeRepository memeRepository;

    @Override
    public Meme salvar(final MultipartFile file, final MemeDto memeDto) {
        try {
            final Meme meme = Meme.builder().nome(memeDto.getNome()).imagem(file.getBytes())
                    .imagemNome(file.getOriginalFilename()).isFake(true).date(new Date()).build();

            return memeRepository.save(meme);
        } catch (final Exception e) {
            throw new BadRequestMeme("Falha carregar image: " + e.getMessage() + " tracer: " + e.getStackTrace());
        }
    }
    
    @Override
    public Meme obterMeme(final Meme meme) {
        return memeRepository.findById(meme.getId())
                .orElseThrow(() -> new BadRequestMeme("Não encontrado id mencionado"));
    }

    @Override
    public List<Meme> obterTodos() {
        return memeRepository.findAll();
    }

   
    public Page<Meme> obterTodos(final Pageable pageable) {
        return memeRepository.findAll(pageable);
    }

    @Override
    public void remover(final Meme meme) {
        memeRepository.delete(meme);
    }


}
