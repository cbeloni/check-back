package com.checkfake.checkback.service;

import java.util.List;

import com.checkfake.checkback.dto.MemeDto;
import com.checkfake.checkback.entity.Meme;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface MemeService{
    public Meme salvar(MultipartFile file, MemeDto memeDto);
    public Meme obterMeme(final Meme meme);
    public List<Meme> obterTodos();
    public Page<Meme> obterTodos(final Pageable pageable);
    public void remover(final Meme meme) ;

}