package com.checkfake.checkback.util;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Component
public class Arquivo {

    @Autowired
    private ResourceLoader resourceLoader;


    public byte[] fileToBase64(String nome) throws IOException {
        Resource resource = resourceLoader.getResource(nome);

        File file = resource.getFile();
        return Base64.encodeBase64(Files.readAllBytes(file.toPath()));
    }
}
