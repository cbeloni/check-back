package com.checkfake.checkback.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.checkfake.checkback.entity.Meme;

public interface MemeRepository extends JpaRepository<Meme,Long>, PagingAndSortingRepository<Meme, Long> {
}
