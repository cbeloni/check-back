package com.checkfake.checkback.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MemeDto {
    private String nome;
}
