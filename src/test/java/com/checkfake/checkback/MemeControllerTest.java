package com.checkfake.checkback;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.List;

import com.checkfake.checkback.controller.MemeController;
import com.checkfake.checkback.dto.MemeDto;
import com.checkfake.checkback.entity.Meme;
import com.checkfake.checkback.repository.MemeRepository;
import com.checkfake.checkback.util.Arquivo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.multipart.MultipartFile;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MemeControllerTest {
	
	@Autowired
	private MemeController memeController;

	@Autowired
	private MemeRepository memeRepository;
	
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private Arquivo arquivo;

	@Before
	public void prepara() throws IOException{
		this.memeRepository.deleteAll();

		String name = "meme_bozo.jpg";
		String originalFileName = "meme_bozo.jpg";
		String contentType = "image/jpeg";
		byte[] content = arquivo.fileToBase64(name);
		
		MultipartFile result = new MockMultipartFile(name,
		                     originalFileName, contentType, content);
		
		MemeDto meme = MemeDto.builder().nome("Meme").build();
		
		this.memeController.salvar(result, meme);
	}
	
	@Test
	public void getMeme() {
		List<Meme> memes = this.memeController.obterTodos();
		Assert.assertEquals(1, memes.size());
	}

	@Test
	public void getMemeHttp() throws Exception{
		this.mockMvc.perform(get("/meme/listar"))
			        .andDo(print())
			        .andExpect(status().isOk());
	}

	@Test
	public void listarPaginado() throws  Exception {
		this.mockMvc.perform(get("/meme/listarPaginado?pageNo=1&pageSize=10&sortBy=id"))
				    .andDo(print())
				    .andExpect(status().isOk());
	}
}
